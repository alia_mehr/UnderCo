<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\SettingsController;
use App\Http\Requests;
use Session;
use Auth;
class AdminController extends Controller
{

    public function login (Request $r) {
    	$method = $r->method();

    	if ($method == 'GET') {
    		return view('admin.login');
    	} elseif ($method == 'POST') {
            $this->validate($r,[
                'adminmail' => 'required|email|exists:users,email',
                'adminpass' => 'required|min:6'
            ]);
            $email = $r->input('adminmail');
            $password = $r->input('adminpass');
            $passSalt = $password . $email;
            if (Auth::attempt(['email' => $email, 'password' => $passSalt])) {
                $this->_sessionUserInfo();
                $tempEmail = SettingsController::get();
                if (Auth::user()->email !== $tempEmail[0]['email_admin']) {
                    session()->put(['you.yourAdmin'=> false]);
                } else {
                    session()->put(['you.yourAdmin'=> true]);
                }
                return redirect()->intended('admin');
            } else {
                return redirect('admin/login');
            }
    	}
	}
    protected function _sessionUserInfo(){
        $tempArr = ['you' =>[
            'yourId' =>Auth::user()->id,
            'yourName' =>Auth::user()->name,
            'yourMail' =>Auth::user()->email,
            'yourChanged' =>Auth::user()->updated_at,
            'yourJoined' =>Auth::user()->created_at]
        ];
        session($tempArr);
    }
    public function logout(){
        // dd(Auth::user());
        session()->flush();
        Auth::logout();
        return redirect('/');
    }

    public function forget (Request $r) {
        $method = $r->method();
        if ($method == 'GET') {
            return view('admin.forget');
        } elseif ($method == 'POST') {
            $this->validate($r,[
                'emailreset' => 'required|email'
            ]);
            $mail = $r->input('emailreset');
            return redirect('admin/login');
        }
	}

    public function help () {
		return view('admin.help');
	}


}
