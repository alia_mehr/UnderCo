<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Newsletter;

class EmailsController extends Controller
{
    private $_emailFields_ = null;

    public function __construct(){
    	$this->_emailFields_ = $this->_get();
    }

    public function index (Request $r) {
    	$method = $r->method();
    	if ($method == 'GET') {
    		return $this->show();
    	} elseif ($method == 'POST') {
            return $this->_removeMail($r);
    	}
	}    

	public function show () {
		return view('admin.emails',['emailList' => $this->_emailFields_, 'i' => '1']);
	}

	protected function _get(){
		$tempData = Newsletter::all();
    	$tempData = $tempData->toArray();
    	return $tempData;
	}

	protected function _removeMail(Request $r){
		$inputEmailDel = $r->all();
		$this->validate($r,[
            'idMail' => 'required|integer|exists:newsletters,id',
			'DeleteMail' => 'regex:/^Delete$/'
        ]);
        session()->put('resultAlertEmail', Newsletter::destroy($inputEmailDel['idMail']));
        return redirect('admin/emails');
	}

}
