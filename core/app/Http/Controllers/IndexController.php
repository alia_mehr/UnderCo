<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\SettingsController;
use App\Http\Controllers\EmailsController;
use App\Http\Controllers\TimerController;
use App\Contact;
use App\Newsletter;
use Auth;
use Session;

class IndexController extends Controller
{
	// private $_filedsName_ = ['namevisitor','emailvisitor','webvisitor','messagevisitor'];
    private $_indexSettingFields_ = null;
    
    public function __construct(){
        $this->_indexSettingFields_ = $this->_getSettingFields();
        $this->initSession('time');
    }

    public function index (Request $r) {
        $method = $r->method();
        if ($method == 'GET') {
            return $this->show();
        } elseif ($method == 'POST') {
            return $this->errspage();
        }
    }

    public function show () {
        if (($this->_indexSettingFields_[0]['delay_time']-time()) > 1){
            return view('index',['indexFields' => $this->_indexSettingFields_ ]);
        } else {
            if (!headers_sent()){
                header('Location: '. $this->_indexSettingFields_[0]['goto_link']); 
                exit;
            } else {
                 echo '<script type="text/javascript">';
                 echo 'window.location.href="'.$this->_indexSettingFields_[0]['goto_link'].'";';
                 echo '</script>';
                 echo '<noscript>';
                 echo '<meta http-equiv="refresh" content="0;url='.$this->_indexSettingFields_[0]['goto_link'].'" />';
                 echo '</noscript>'; 
                 exit;
            }
        }
    }

    protected function _getSettingFields(){
         return SettingsController::get();
    }

    public function contact(Request $r, $type){
    	if ($type=='comment' && $this->_indexSettingFields_[0]['amessage']) {
    		$this->validate($r,[
                'namevisitor' => 'required|min:4|alpha_num',
                'emailvisitor' => 'required|email',
                'webvisitor' => 'url',
                'messagevisitor' => 'required|min:10'
            ]);
            $this->saveComment($r);
    	} elseif ($type == 'email' && $this->_indexSettingFields_[0]['anewsletter']) {
    		$this->validate($r,[
                'typewilling' => 'required|regex:/Subscribe$/i', 
                'emailwilling' => 'required|email'
            ]);
            $this->saveEmail($r);
    	} else {
    		return redirect('error');
    	}
    	return redirect('/#contact');
    }

    protected function saveComment(Request $r){
        $inputComment = $r->all();
        $newComment = new Contact;

        $newComment->name = $inputComment['namevisitor'];
        $newComment->email = $inputComment['emailvisitor'];
        $newComment->web = $inputComment['webvisitor'];
        $newComment->content = $inputComment['messagevisitor'];

        $tempTime = dateTime_random('real');

        $newComment->created_at = $tempTime;
        $newComment->updated_at = $tempTime;
        // dd($newComment);
        session()->put('resultGetMsgorEmail', $newComment->save());
    }

    protected function saveEmail(Request $r){
        $inputEmail = $r->all();
        if ($inputEmail['typewilling'] === 'Subscribe') {
            $this->validate($r,[
                'emailwilling' => 'unique:newsletters,email'
            ]);
            $newEmail = new Newsletter;
            $newEmail->email = $inputEmail['emailwilling'];
            $tempTime = dateTime_random('real');
            $newEmail->created_at = $tempTime;
            $newEmail->updated_at = $tempTime;
            
            $newEmail = $newEmail->save();
        } else {
            $this->validate($r,[
                'emailwilling' => 'exists:newsletters,email'
            ]);
            $newEmail = Newsletter::where('email', $inputEmail['emailwilling'])->delete();
        }
        session()->put('resultGetMsgorEmail', $newEmail);
    }


    public function adminPage () {
        if (Auth::check()) {
            if(session('you.yourAdmin')){
                $this->initSession('status');
            }
        }
        return view('admin.index');
    }

    protected function initSession($type){
        switch ($type) {
            case 'time':
                session()->put('timer', TimerController::convert($this->_indexSettingFields_[0]['delay_time']));
                session()->put('maintimer', $this->_indexSettingFields_[0]['delay_time'] - time() - 1);
            break;
            case 'status':
                session()->put('totalMsg', count(Contact::all()));
                session()->put('totalMail', count(Newsletter::all()));
            break;
            
            default:
                $this->errspage();
            break;
        }

        

    }

    public function errspage(){
        return view('errors.epage');
    }

    public function test(){
        return phpinfo();
    }
}
