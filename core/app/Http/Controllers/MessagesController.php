<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Contact;

class MessagesController extends Controller
{


	    private $_msgFields_ = null;

    public function __construct(){
    	$this->_msgFields_ = $this->_get();
    }

    public function index (Request $r) {
    	$method = $r->method();
    	if ($method == 'GET') {
    		return $this->show();
    	} elseif ($method == 'POST') {
            return $this->_removeMsg($r);
    	}
	}    

	public function show () {
		return view('admin.messages',['msgList' => $this->_msgFields_, 'i' => '1']);
	}

	protected function _get(){
		$tempData = Contact::all();
    	$tempData = $tempData->toArray();
    	return $tempData;
	}

	protected function _removeMsg(Request $r){
		$inputMsgDel = $r->all();
		$this->validate($r,[
            'idMsg' => 'required|integer|exists:contacts,id',
			'DeleteMsg' => 'regex:/^Delete$/'
        ]);
        session()->put('resultAlertMsg', Contact::destroy($inputMsgDel['idMsg']));
        return redirect('admin/messages');
	}

}
