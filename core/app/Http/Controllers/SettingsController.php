<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\TimerController;
use App\Setting;

class SettingsController extends Controller
{
    private $_settingFields_ = null;

    public function __construct(){
    	$this->_settingFields_ = $this->get();
    }

    public function index (Request $r) {
        $method = $r->method();
        if ($method == 'GET') {
            return $this->show();
        } elseif ($method == 'POST') {
            return $this->_set($r);
        }
    }

    public function show () {
		return view('admin.settings',['settingField' => $this->_settingFields_]);
	}

	public static function get()
	{
		$tempData = Setting::all();
    	$tempData = $tempData->toArray();
        // $tempData[0]['delay_time'] = TimerController::convert($tempData[0]['delay_time']);
    	$tempData[0]['parts_time'] = explode(':', TimerController::convert($tempData[0]['delay_time'],'NoZero'));
    	$tempData[0]['day'] = ($tempData[0]['parts_time'][0] < 0) ? 0 : $tempData[0]['parts_time'][0];
    	$tempData[0]['hour'] = ($tempData[0]['parts_time'][1] < 0) ? 0 : $tempData[0]['parts_time'][1];
    	$tempData[0]['minute'] = ($tempData[0]['parts_time'][2] < 0) ? 0 : $tempData[0]['parts_time'][2];
    	$tempData[0]['second'] = ($tempData[0]['parts_time'][3] < 0) ? 0 : $tempData[0]['parts_time'][3];
    	$tempData[0]['parts_contact_status'] = explode('|', $tempData[0]['contact_status']);
    	$tempData[0]['amessage'] = $tempData[0]['parts_contact_status'][0];
    	$tempData[0]['anewsletter'] = $tempData[0]['parts_contact_status'][1];
    	return $tempData;
	}

    protected function _set (Request $r) {
		$this->validate($r,[
            'day' => 'required|min:0|integer|max:999999',
			'hour' => 'required|integer|min:0|max:23',
			'minute' => 'required|integer|min:0|max:59',
			'second' => 'required|integer|min:0|max:59',
			'title' => 'required|string|between:5,19',
			'headertext' => 'required|string|between:10,49',
			'description' => 'required|string|between:200,4000000',
			'link' => 'required|url|between:6,79',
			'amessage' => 'boolean',
			'anewsletter' => 'boolean',
			'mailadd' => 'required|email|exists:users,email',
			'commander' => 'regex:/^ST/'
        ]);
        $inputSetting = $r->all();
        switch ($inputSetting['commander']) {
        	case 'START':
        		$tempTime = TimerController::convert($inputSetting['day'] .':'. $inputSetting['hour'] .':'. $inputSetting['minute'] .':'. $inputSetting['second']);
                
        	break;
        	case 'STOP':
        		$tempTime = '0';
        	break;
        	default:
        		return redirect()->route('pageError');
        	break;
        }
        $tempContact = (isset($inputSetting['amessage']) ? '1' : '0') .'|'. (isset($inputSetting['anewsletter']) ? '1' : '0');
        $positioner = Setting::find(1);
			$positioner->email_admin = $inputSetting['mailadd'];
			$positioner->delay_time = $tempTime;
			$positioner->title_site = $inputSetting['title'];
			$positioner->header_site = $inputSetting['headertext'];
			$positioner->description_site = $inputSetting['description'];
			$positioner->goto_link = $inputSetting['link'];
			$positioner->contact_status = $tempContact;
			$positioner->updated_at = dateTime_random('real');
		session()->put('resultAlertSetting', $positioner->save());
        return redirect('admin/settings');
	}
}
