<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TimerController extends Controller
{
    //
    //
	public static function convert($timeString, $type = 'ByZero'){
    	// 12:14:16:18 => 123213113
    	// 123213113 => 12:14:16:18
        //ByZero => 00:00:00:00 //    i.e. for index controller
        //NoZero => 0:0:0:0 //    i.e. for setting controller 
		if (strpos($timeString, ':') != false) {
			$tempTime = explode(':',$timeString);
			$tempString = "+$tempTime[0] days $tempTime[1] hours $tempTime[2] minutes $tempTime[3] seconds";
			return strtotime($tempString);
		} else {
			$day = 0 ;
			$hour = 0;
			$minute = 0;
			$tempTime = $timeString - time();
    		$second = ($tempTime > 0 ) ? $tempTime : 0;				
    		while ($second > 59){
    			$minute++;
    			$second = $second - 60;
    			if($second < 0){
    				$second = $second * (-1);
    			}	
    		}
    		while ($minute > 59){
    			$hour++;
    			$minute = $minute - 60;
    			if($minute < 0){
    				$minute = $minute * (-1);
    			}
    		}
    		while ($hour > 23){
    			$day++;
    			$hour = $hour - 24;
    			if($hour<0){
    				$hour = $hour * (-1);
    			}
    		}
            switch ($type) {
                case 'NoZero':
                    return $day . ':' . $hour . ':' . $minute . ':' . $second;
                break;
                case 'ByZero':
                    return (($day < 10) ? "0" . $day : $day) . ':' . (($hour < 10) ? "0" . $hour : $hour) . ':' . (($minute < 10) ? "0" . $minute : $minute) . ':' . (($second < 10) ? "0" . $second : $second);
                break;
            }
        }
    }
}
