<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use App\User;

class UserController extends Controller
{
    //
   	private $_userFields_ = null;

    public function __construct(){
    	$this->_userFields_ = $this->_get();
    }

    public function index (Request $r) {
    	$method = $r->method();
    	if ($method == 'GET') {
    		return $this->show();
    	} elseif ($method == 'POST') {
    		$this->validate($r,[
	            'idUser' => 'required|integer|exists:users,id'
        	]);
            return $this->_editUser($r);
    	}
	}    

	public function show () {
		return view('admin.users',['userList' => $this->_userFields_, 'i' => '1']);
	}

	protected function _get(){
		$tempData = User::all();
    	$tempData = $tempData->toArray();
        foreach ($tempData as $key => $value) {
            if($value['email'] === session('you.yourMail')){
                $keyPlus = ($key == 0) ? ($key+1) : $key;
                array_splice($tempData, $key, $keyPlus);
            }
        }
    	return $tempData;
	}

	protected function _editUser(Request $r){
		$inputMember = $r->all();

        if(array_key_exists('EditUser', $inputMember)){
            $this->validate($r,[
                'EditUser' => 'regex:/^Save/'
            ]);
            $upInfo = User::find($inputMember['idUser']);
            $flag = false;
            if ($inputMember['email'] !== $upInfo->email) {
                $this->validate($r,[
                        'email' => 'required|email|unique:users,email'
                    ]);
                $upInfo->email = $inputMember['email'];
                $flag = true;
            }
            if ($inputMember['password'] !== '') {
                $this->validate($r,[
                    'password' => 'confirmed|between:6,30'
                    ]);
                $passSalt = $inputMember['password'] . $inputMember['email'];
                $upInfo->password = bcrypt($passSalt);
                $flag = true;
            }

            if ($inputMember['name'] !== $upInfo->name) {
                $this->validate($r,[
                        'name' => 'required|alpha_dash|between:4,30'
                    ]);
                $upInfo->name = $inputMember['name'];
                $flag = true;
            }
            if($flag){
                $tempTime = dateTime_random('real');
                $upInfo->updated_at = $tempTime;
                $tempResult = $upInfo->save();
            }
        } elseif (array_key_exists('DeleteUser', $inputMember)) {
            $this->validate($r,[
                'DeleteUser' => 'regex:/^Delete$/'
            ]);
           $tempResult = User::destroy($inputMember['idUser']);
        }
        session()->put('resultAlertuser', $tempResult);
        return redirect('admin/user');
	}

	public function add (Request $r) {
    	$method = $r->method();
    	if ($method == 'GET') {
    		return view('admin.addusers');
    	} elseif ($method == 'POST') {
    		$this->validate($r,[
	            'name' => 'required|alpha_dash|between:4,30',
				'email' => 'required|email|unique:users,email',
				'password' => 'required|confirmed|between:6,30'
        	]);
            return $this->_addUser($r);
    	}
	}

	protected function _addUser(Request $r){
        $inputMember = $r->all();
        $newMember = new User;

        $newMember->name = $inputMember['name'];
        $newMember->email = $inputMember['email'];

        $passSalt = $inputMember['password'] . $inputMember['email'];
        $newMember->password = bcrypt($passSalt);

        $tempTime = dateTime_random('real');
        $newMember->created_at = $tempTime;
        $newMember->updated_at = $tempTime;
        // dd($newMember);
        session()->put('resultAlertAddUser', $newMember->save());
        return redirect('admin/adduser');
	}


    public function editinfo(Request $r){
        $inputMember = $r->all();
        $this->validate($r,[
                'idUser' => 'required|integer|exists:users,id',
                'EditUser' => 'regex:/^Save/'
            ]);
        $upInfo = User::find($inputMember['idUser']);
        $flag = false;
        if (!session('you.yourAdmin')) {
            if ($inputMember['email'] !== $upInfo->email) {
                $this->validate($r,[
                        'email' => 'required|email|unique:users,email'
                    ]);
                $upInfo->email = $inputMember['email'];
                session()->put(['you.yourMail'=> $inputMember['email']]);
                $flag = true;
            }
        }
        if ($inputMember['password'] !== '') {
            $this->validate($r,[
                'password' => 'confirmed|between:6,30'
                ]);
            $tempMail = (session('you.yourAdmin')) ? session('you.yourMail') : $inputMember['email'];
            $passSalt = $inputMember['password'] . $tempMail;
            $upInfo->password = bcrypt($passSalt);
            $flag = true;
        }

        if ($inputMember['name'] !== $upInfo->name) {
            $this->validate($r,[
                    'name' => 'required|alpha_dash|between:4,30'
                ]);
            $upInfo->name = $inputMember['name'];
            session()->put(['you.yourName'=> $inputMember['name']]);
            $flag = true;
        }
        if($flag){
            $tempTime = dateTime_random('real');
            $upInfo->updated_at = $tempTime;
            session()->put(['you.yourChanged'=> $tempTime]);
            session()->put('resultAlertEditInfo', $upInfo->save());
        }
        return redirect('admin');
    }

}
