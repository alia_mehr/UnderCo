<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Http\Controllers\SettingsController;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tempEmail = SettingsController::get();
        if (Auth::user()->email !== $tempEmail[0]['email_admin']) {
            session()->put(['you.yourAdmin'=> false]);
            return redirect('admin');
        }
        session()->put(['you.yourAdmin'=> true]);
        return $next($request);
    }
}
