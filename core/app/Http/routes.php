<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', ['uses'=>'IndexController@index']);
Route::post('contact/{type}', ['uses'=>'IndexController@contact']);
Route::any('error', ['as' => 'pageError','uses'=>'IndexController@errspage']);
Route::match(['get','post'],'admin/login', ['uses'=>'AdminController@login']);
//Route::match(['get','post'],'admin/forget', ['uses'=>'AdminController@forget']);
Route::match(['get','post'],'admin/logout', ['uses'=>'AdminController@logout']);



Route::group(['middleware' => 'auth'], function () {
	Route::get('admin', ['uses'=>'IndexController@adminPage']);
	// Route::get('admin/help', ['uses'=>'AdminController@help']);
	Route::post('admin/editinfo', ['uses'=>'UserController@editinfo']);
	Route::match(['get','post'],'admin/user', ['middleware' => 'isAdmin','uses'=>'UserController@index']);
	Route::match(['get','post'],'admin/adduser', ['middleware' => 'isAdmin','uses'=>'UserController@add']);
	Route::match(['get','post'],'admin/emails', ['middleware' => 'isAdmin','uses'=>'EmailsController@index']);
	Route::match(['get','post'],'admin/messages', ['middleware' => 'isAdmin','uses'=>'MessagesController@index']);
	Route::match(['get','post'],'admin/settings', ['middleware' => 'isAdmin','uses'=>'SettingsController@index']);
});

// Route::auth();
