<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email_admin',255);
            $table->string('delay_time',16);
            $table->string('title_site',20);
            $table->string('header_site',50);
            $table->mediumText('description_site');
            $table->string('goto_link',80);
            $table->string('contact_status',3);
            $table->timestamps();
            $table->foreign('email_admin')->references('email')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
