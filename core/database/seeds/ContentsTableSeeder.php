<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 31 ; $i++) { 
    		DB::table('contacts')->insert([
    			'name' => str_random(12). ' ' . str_random(15),
    			'email' => str_random(10).'@'. str_random(5) . '.' . str_random(3),
    			'web' => str_random(10).'.'. str_random(3),
    			'content' => str_random(19).' '. str_random(55) . ' ' . str_random(120) . '.' . '....end....',
    			'created_at' => dateTime_random(),
    			'updated_at' => dateTime_random()
    			]);
    	}
    }
}
