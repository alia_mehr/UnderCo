<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

class NewslettersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=1; $i < 31 ; $i++) { 
    		DB::table('newsletters')->insert([
    			'email' => str_random(10).'@'. str_random(5) . '.' . str_random(3),
    			'created_at' => dateTime_random(),
    			'updated_at' => dateTime_random()
    			]);
    	}
    }
}
