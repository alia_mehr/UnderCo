<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
			'email_admin' => 'underco@alia.gq',
			'delay_time' => '124:05:12:14',
			'title_site' => 'UnderCo Script',
			'header_site' => 'Under Construction',
			'description_site' => 'This script can be downloaded from the following site. ( site: alia.gq ) Lorem ipsum Eu exercitation laborum veniam reprehenderit nostrud deserunt laborum sit sint Excepteur Ut adipisicing mollit incididunt culpa et incididunt ut reprehenderit non officia consequat sit aliqua cillum nostrud. Lorem ipsum Consequat est occaecat magna do non consequat esse irure ullamco ad.',
			'goto_link' => 'http://alia.gq/',
			'contact_status' => '1|1',
			'created_at' => dateTime_random('real'),
			'updated_at' => dateTime_random('real')
        	]);
    }
}
