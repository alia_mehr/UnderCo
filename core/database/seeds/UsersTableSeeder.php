<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
			'name' => 'I am a admin',
			'email' => 'underco@alia.gq',
			'password' => bcrypt('123456underco@alia.gq'),
			'remember_token' => '',
			'created_at' => dateTime_random('real'),
			'updated_at' => dateTime_random('real')
        	]);
            DB::table('users')->insert([
            'name' => 'I am a user',
            'email' => 'user_underco@alia.gq',
            'password' => bcrypt('123456user_underco@alia.gq'),
            'remember_token' => '',
            'created_at' => dateTime_random('real'),
            'updated_at' => dateTime_random('real')
            ]);
    }
}
