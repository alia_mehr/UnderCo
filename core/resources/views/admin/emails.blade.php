@extends('theme.mainpage')
@section('statusEmail', 'active')


@section('body_page')
<div class="col-md-8 col-md-offset-2">
	<div class="jumbotron">
		@if (session()->pull('resultAlertEmail'))
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>OK!</strong> The operation was successful.
			</div>
		@endif
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>ID</th>
					<th>Time</th>
					<th>Email</th>
				</tr>
			</thead>

			@foreach ($emailList as $item => $value)
				<tbody>
					<tr data-toggle="modal" data-target="#mailModal{{ $value['id'] }}">
						<td>{{ $i++ }}</td>
						<td>{{ $value['id'] }}</td>
						<td>{{ $value['created_at'] }}</td>
						<td>{{ $value['email'] }}</td>
					</tr>
				</tbody>

				<div class="modal fade" id="mailModal{{ $value['id'] }}" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Email</h4>
							</div>
							<div class="modal-body">
								<p class="wordwraptrue">{{ $value['email'] }}</p>
							</div>
							<div class="modal-footer">
								<div class="row">

									<div class="col-md-2">
										ID : {{ $value['id'] }}
									</div>

									<div class="col-md-6">
										Time : {{ $value['created_at'] }}
									</div>

									<div class="col-md-4">
										<form method="post" action="">
											<input type="hidden" name="idMail" value="{{ $value['id'] }}">
											{!! csrf_field() !!}
											<input type="submit" class="btn btn-danger" name="DeleteMail" value="Delete">
										</form>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</table>
	</div>			
</div>

@endsection
