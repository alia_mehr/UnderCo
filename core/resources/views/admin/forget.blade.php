@extends('theme.mainpage')
@section('body_page')
@if (count($errors) > 0)
	@foreach ($errors->all() as $error)
		@if (strpos($error, 'emailreset') > 0) 
	    	@section('emailreset','Error: ' . str_replace('emailreset', 'Email', $error))
		@endif
	@endforeach
@endif
<div class="col-md-4 col-md-offset-4" id="sitestatus">
	<div class="panel panel-default">
		<div class="panel-heading"><span class="glyphicon glyphicon-repeat"></span>      &shy;Reset Password</div>
		<div class="panel-body">
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group">
							<span class="input-group-addon">Email</span>
							<input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" name="emailreset" id="emailreset" value="{{ old('emailreset') }}">
						</div>
						<div style="color:red;">@yield('emailreset')</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2">
						<div class="input-group">
							<a href="../admin/login"><button type="button" class="btn btn-link">Login to Site</button></a>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-success btn-block">Reset Password</button>
				{!! csrf_field() !!}
			</form>
		</div>
	</div>			
</div>
@endsection

