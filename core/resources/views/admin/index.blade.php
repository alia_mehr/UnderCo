@extends('theme.mainpage')
@section('body_page')
<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-primary" id="sitestatus">
		<div class="panel-heading" style="font-size:12pt;"><span class="glyphicon glyphicon-user"></span>   Welcome {{ session('you.yourAdmin')? 'Admin' : 'User' }}<span class="pull-right glyphicon glyphicon-edit" data-toggle="modal" data-target="#userModal"></span></div>
		<div class="panel-body">
			@if (session()->pull('resultAlertEditInfo'))
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>OK!</strong> The operation was successful.
				</div>
			@endif
			<div class="alert alert-success">
				<strong>Remaining Time: </strong><i class="resulttime"> {{ session('timer') }} </i>
			</div>

			<div class="alert alert-info">
				<strong>Name: </strong> {{ session('you.yourName') }}
			</div>
			
			<div class="alert alert-info">
				<strong>Email: </strong>  {{ session('you.yourMail') }}
			</div>

			<div class="alert alert-info">
				<strong>Change your info at:</strong>  {{ session('you.yourChanged') }}
			</div>

			<div class="alert alert-info">
				<strong>Join at:</strong>  {{ session('you.yourJoined') }}
			</div>
		</div>
	</div>			
</div>
<div class="modal fade bs-example-modal-sm" id="userModal" role="dialog" tabindex="-1" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Acount Info</h4>
			</div>
			<form class="form-horizontal" role="form" method="POST" action="../admin/editinfo">
				<div class="modal-body">

					{{ csrf_field() }}
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="name" class="control-label">Name</label>
							<input id="name" type="text" class="form-control" name="name" value="{{ session('you.yourName') }}">
							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>
					
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="email" class="control-label">E-Mail Address</label>
							<input id="email" type="email" class="form-control" name="email" value="{{ session('you.yourMail') }}" {{ session('you.yourAdmin') ? 'disabled' : '' }}>
							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="password" class="control-label">Password</label>
							<input id="password" type="password" class="form-control" name="password">
							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="password-confirm" class="control-label">Confirm Password</label>
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
							@if ($errors->has('password_confirmation'))
							<span class="help-block">
								<strong>{{ $errors->first('password_confirmation') }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<input type="hidden" name="idUser" value="{{ session('you.yourId') }}">
					<input type="submit" class="btn btn-success" name="EditUser" value="Save Change">
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

