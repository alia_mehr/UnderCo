@extends('theme.mainpage')
@section('body_page')

@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        @if (strpos($error, 'adminmail') > 0)
        	@section('errmail','has-error')
        	@section('errtextmail')
        		<div>Error: {{ str_replace('adminmail', 'Email', $error) }}</div>
        	@endsection
        @endif

        @if (strpos($error, 'adminpass') > 0)
        	@section('errpass','has-error')
        	@section('errtextpass')
        		<div>Error: {{ str_replace('adminpass', 'Password', $error) }}</div>
        	@endsection
        @endif
   	@endforeach
@endif

<div class="col-md-4 col-md-offset-4" id="sitestatus">
	<div class="panel panel-default">
		<div class="panel-heading"><span class="glyphicon glyphicon-log-in"></span>      &shy;login</div>
		<div class="panel-body">
			
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group @yield('errmail')">
							<span class="input-group-addon">Email</span>
							<input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" name="adminmail" value= {{ old('adminmail') }}>
						</div>
						@yield('errtextmail')
						<span id="inputSuccess3Status" class="sr-only">(success)</span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group @yield('errpass')">
							<span class="input-group-addon">Password</span>
							<input type="password" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" name="adminpass">
						</div>
						@yield('errtextpass')
					</div>
				</div>
				{{-- <div class="form-group">
					<div class="col-md-2">
						<div class="input-group">
							 <a href="../admin/forget"><button type="button" class="btn btn-link">Forget Password</button></a> 
						</div>
					</div>
				</div> --}}
				<button type="submit" class="btn btn-success btn-block">Login to site</button>
				{!! csrf_field() !!}
			</form>
		</div>
	</div>			
</div>

@endsection