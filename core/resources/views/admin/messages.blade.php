@extends('theme.mainpage')
@section('tmessage', count($msgList))
@section('statusMessage', 'active')
@section('body_page')
<div class="col-md-8 col-md-offset-2">
	<div class="jumbotron">
		@if (session()->pull('resultAlertMsg'))
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>OK!</strong> The operation was successful.
			</div>
		@endif
		<table class="table table-striped">
			<thead> 
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Message</th>
				</tr>
			</thead>
			@foreach ($msgList as $item => $value)
				<tbody>
					<tr title="Email:  {{ $value['email'] }} " data-toggle="modal" data-target="#msgModal{{ $value['id'] }}">
						<td>{{ $i++ }}</td>
						<td>{{ $value['name'] }}</td>
						<td>{{ substr($value['content'],0,40) }} ...</td>
					</tr>
				</tbody>

<!-- Modal -->
<div class="modal fade" id="msgModal{{ $value['id'] }}" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ $value['name'] }} <small>  {{ $value['email'] }}</small></h4>
			</div>
			<div class="modal-body">
				<p class="wordwraptrue">{{ $value['content'] }}</p>
				<small>ID : {{ $value['id'] }}</small>
			</div>
			<div class="modal-footer">
				<div class="row">

					<div class="col-md-2">
						<a href="{{ $value['web'] }}" target="_blank">Web Link</a>
					</div>

					<div class="col-md-6">
						Time : {{ $value['created_at'] }}
					</div>

					<div class="col-md-4">
						<form method="post" action="">
							<input type="hidden" name="idMsg" value="{{ $value['id'] }}">
							{!! csrf_field() !!}
							<input type="submit" class="btn btn-danger" name="DeleteMsg" value="Delete">
						</form>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>


			@endforeach
		</table>
	</div>			
</div>

@endsection

