@extends('theme.mainpage')

@section('statusMnueSetting', 'active')
@section('statusSetting', 'active')
@section('body_page')
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
		@if (strpos($error, 'day') > 0) 
        	@section('day','Error: ' . str_replace('day', 'Day', $error))
        @elseif (strpos($error, 'hour') > 0) 
			@section('hour','Error: ' . str_replace('hour', 'Hour', $error))
        @elseif (strpos($error, 'minute') > 0) 
			@section('minute','Error: ' . str_replace('minute', 'Minute', $error))
        @elseif (strpos($error, 'second') > 0) 
			@section('second','Error: ' . str_replace('second', 'Second', $error))
        @elseif (strpos($error, 'title') > 0) 
			@section('title','Error: ' . str_replace('title', 'Title', $error))
        @elseif (strpos($error, 'headertext') > 0) 
			@section('headertext','Error: ' . str_replace('headertext', 'Header', $error))
        @elseif (strpos($error, 'description') > 0) 
			@section('description','Error: ' . str_replace('description', 'Description', $error))
        @elseif (strpos($error, 'link') > 0) 
			@section('link','Error: ' . str_replace('link', 'Link', $error))
        @elseif (strpos($error, 'amessage') > 0) 
			@section('amessage','Error: ' . str_replace('amessage', 'Active Message', $error))
        @elseif (strpos($error, 'anewsletter') > 0) 
			@section('anewsletter','Error: ' . str_replace('anewsletter', 'Active Newsletter', $error))
        @elseif (strpos($error, 'mailadd') > 0) 
			@section('mailadd','Error: ' . str_replace('mailadd', 'Email', $error))
		@elseif (strpos($error, 'commander') > 0) 
			@section('commander','Error: ' . str_replace('commander', 'Request', $error))
		@endif
   	@endforeach
@endif
<div class="col-md-8 col-md-offset-2">
	<div class="jumbotron">

	@if (session()->pull('resultAlertSetting'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>OK!</strong> The operation was successful.
		</div>
	@endif

		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<form method="post" action="">
					<div class="row form-group" id="timerset">
						<div class="col-sm-3 col-xs-6">
							<label for="day">Day</label>
							<i data-toggle="tooltip" title="What it takes is your site ready? i.e. 14:12:5:18" class="glyphicon glyphicon-question-sign"></i>
							<input class="form-control" id="day" name="day" type="text" value="{{ $settingField[0]['day'] }}">
							<div style="color:red;">@yield('day')</div>
						</div>
						<div class="col-sm-3 col-xs-6">
							<label for="hour">Hour</label>
							<input class="form-control" id="hour" name="hour" type="text" value="{{ $settingField[0]['hour'] }}">
							<div style="color:red;">@yield('hour')</div>
						</div>
						<div class="col-sm-3 col-xs-6">
							<label for="minute">Minute</label>
							<input class="form-control" id="minute" name="minute" type="text" value="{{ $settingField[0]['minute'] }}">
							<div style="color:red;">@yield('minute')</div>
						</div>
						<div class="col-sm-3 col-xs-6">
							<label for="second">Second</label>
							<input class="form-control" id="second" name="second" type="text" value="{{ $settingField[0]['second'] }}">
							<div style="color:red;">@yield('second')</div>
						</div>
					</div>

					<div class="row form-group" id="texts">
						<div class="col-xs-12">
							<label for="title">Title</label>
							<input class="form-control" id="title" name="title" type="text" value="{{ $settingField[0]['title_site'] }}">
							<div style="color:red;">@yield('title')</div>
						</div>
						<div class="col-xs-12">
							<label for="headertext">Header</label>
							<input class="form-control" id="headertext" name="headertext" type="text" value="{{ $settingField[0]['header_site'] }}">
							<div style="color:red;">@yield('headertext')</div>
						</div>
						<div class="col-xs-12">
							<label for="description">Description</label>
							<textarea class="form-control" rows="5" id="description" name="description">{{ $settingField[0]['description_site'] }}</textarea>
							<div style="color:red;">@yield('description')</div>
						</div>
						<div class="col-xs-12">
							<label for="link">Link</label>
							<i data-toggle="tooltip" title="After the counter is transmitted to this address" class="glyphicon glyphicon-question-sign"></i>
							<input class="form-control" id="link" name="link" type="text" value="{{ $settingField[0]['goto_link'] }}">
							<div style="color:red;">@yield('link')</div>
						</div>
					</div>

					<div class="row form-group" id="interact">
						<div class="checkbox col-xs-12">
							<label><input type="checkbox" name="amessage" value="1" {{ $settingField[0]['amessage'] ? 'checked' : '' }}>Active message</label>
							<i data-toggle="tooltip" title="Do you want to receive Message?" class="glyphicon glyphicon-question-sign"></i>
							<div style="color:red;">@yield('amessage')</div>
						</div>
						<div class="checkbox col-xs-12">
							<label><input type="checkbox" name="anewsletter" value="1" {{ $settingField[0]['anewsletter'] ? 'checked' : '' }}>Active newsletter</label>
							<i data-toggle="tooltip" title="Do you want to receive E-mail?" class="glyphicon glyphicon-question-sign"></i>
							<div style="color:red;">@yield('anewsletter')</div>
						</div>
					</div>

					<div class="row form-group" id="userdata">
						<div class="col-xs-12">
							<label for="mailadd">Email</label>
							<i data-toggle="tooltip" title="Administrator e-mail account you want to enter in this field" class="glyphicon glyphicon-question-sign"></i>
							<input class="form-control" id="mailadd" name="mailadd" type="text" value="{{ $settingField[0]['email_admin'] }}">
							<div style="color:red;">@yield('mailadd')</div>
						</div>
					</div>

					<div class="row form-group" id="buttons">
						<div class="col-xs-3 col-xs-offset-3">
							<input type="submit" class="btn btn-success" name="commander" value="START">
						</div>
						<div class="col-xs-3">
							<input type="submit" class="btn btn-danger" name="commander" value="STOP">
						</div>
					</div>
					<div style="color:red;">@yield('commander')</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
