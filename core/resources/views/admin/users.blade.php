@extends('theme.mainpage')
@section('statusMnueSetting', 'active')
@section('statusUser', 'active')

@section('body_page')
<div class="col-md-8 col-md-offset-2">
	<div class="jumbotron">
		@if (session()->pull('resultAlertuser'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>OK!</strong> The operation was successful.
		</div>
		@endif
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Time</th>
					<th>Email</th>
				</tr>
			</thead>

			@foreach ($userList as $item => $value)
			<tbody>
				<tr data-toggle="modal" data-target="#userModal{{ $value['id'] }}">
					<td>{{ $i++ }}</td>
					<td>{{ $value['name'] }}</td>
					<td>{{ $value['created_at'] }}</td>
					<td>{{ $value['email'] }}</td>
				</tr>
			</tbody>
			@endforeach
		</table>
	</div>			
</div>

@foreach ($userList as $item => $value)
<div class="modal fade" id="userModal{{ $value['id'] }}" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Info <small>{{ $value['name'] }}</small></h4>
			</div>

			<form class="form-horizontal" role="form" method="POST" action="">
				<div class="modal-body">
					{{ csrf_field() }}
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="name" class="control-label">Name</label>
							<input id="name" type="text" class="form-control" name="name" value="{{ $value['name'] }}">
							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="email" class="control-label">E-Mail Address</label>
							<input id="email" type="email" class="form-control" name="email" value="{{ $value['email'] }}">
							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="password" class="control-label">Password</label>
							<input id="password" type="password" class="form-control" name="password">
							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<label for="password-confirm" class="control-label">Confirm Password</label>
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
							@if ($errors->has('password_confirmation'))
							<span class="help-block">
								<strong>{{ $errors->first('password_confirmation') }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
			
			<div class="modal-footer">
				<input type="submit" class="btn btn-danger pull-left" name="DeleteUser" value="Delete">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<input type="hidden" name="idUser" value="{{ $value['id'] }}">
				<input type="submit" class="btn btn-success" name="EditUser" value="Save Change">
			</div>
			</form>
		</div>
	</div>
</div>
@endforeach


@endsection
