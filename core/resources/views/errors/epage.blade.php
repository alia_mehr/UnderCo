<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/main.css">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<body class="container" id="errpage">
	<div class="row" id="main">
		<div class="col-md-8 col-md-offset-2 marginofbottom">
			<div class="marginofbottom">
				<p class="text-center"><i class="glyphicon glyphicon-warning-sign"></i></p>
				<h1 class="text-center"><strong> Error 404 </strong></h1>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione, dolorem, adipisci blanditiis deleniti</p>
		</div>
		<div class="col-md-4 col-md-offset-4">
				<ul class="nav nav-pills">
					<li role="presentation"><a href="/">Back To Home</a></li>
					<li role="presentation"><a href="../admin/login">I am Admin</a></li>
					<li role="presentation"><a href="/#contact">Contact Us</a></li>
				</ul>
			</div>
		<div class="col-md-2"></div>
	</div>
</body>
</html>