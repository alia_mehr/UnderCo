<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ $indexFields[0]['title_site'] }}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/main.css">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<body class="container-fluid" data-spy="scroll" data-target=".navbar" data-offset="50">
	<div class="row">

	<input type="hidden" id="mainTimer" value="{{ session('maintimer') }}">
		<header>
			<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">{{ $indexFields[0]['title_site'] }}</a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav">
							<li><a href="#titletimer">Timer</a></li>
							<li><a href="#description">Description</a></li>
							<li><a href="#contact">Contact Us</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="resulttime badge">{{ session('timer') }}</span> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="../admin/login"><span class="glyphicon glyphicon-user"></span> admin schema</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<article>
			<div id="titletimer" class="container-fluid">
				<h1 class="text-center" id="title">{{ $indexFields[0]['header_site'] }}</h1>
				<p class="text-center" id="timer"><em class="resulttime">{{ session('timer') }}</em></p>
			</div>
			<div id="description" class="container-fluid">
				<h2 id="headertext">Description</h2>
				<p id="descriptiontext">{{ $indexFields[0]['description_site'] }}</p>
			</div>
			<div id="contact" class="container-fluid">
				<h2 id="contacttext">Contact Us</h2>


				@if (count($errors) > 0)
				    @foreach ($errors->all() as $error)
						@if (strpos($error, 'namevisitor') > 0) 
				        	@section('namevisitor','Error: ' . str_replace('namevisitor', 'Name', $error))
				        @elseif (strpos($error, 'emailvisitor') > 0) 
				        	@section('emailvisitor','Error: ' . str_replace('emailvisitor', 'Email', $error))
				        @elseif (strpos($error, 'webvisitor') > 0) 
				        	@section('webvisitor','Error: ' . str_replace('webvisitor', 'Web', $error))
						@elseif (strpos($error, 'messagevisitor') > 0) 
							@section('messagevisitor', 'Error: ' . str_replace('messagevisitor', 'Message', $error))
						@elseif (strpos($error, 'typewilling') > 0) 
				        	@section('typewilling','Error: ' . str_replace('typewilling', 'Type', $error))
				        @elseif (strpos($error, 'emailwilling') > 0) 
				        	@section('emailwilling','Error: ' . str_replace('emailwilling', 'Email', $error))
						@endif
				   	@endforeach
				@endif


				@if (session()->pull('resultGetMsgorEmail'))
					<div class="col-md-6 col-md-offset-3">
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>OK!</strong> Is successfully received.
						</div>
					</div>
					
				@endif


				<div class="row">
					@if (!$indexFields[0]['amessage'] && !$indexFields[0]['anewsletter'])
						<h3 class="text-center">Oh sorry<p>Now we can not respond to you</p></h3>
					@endif
					@if ($indexFields[0]['amessage'])
					<form class="col-md-10 col-md-offset-1" method="post" action="contact/comment">
						<div class="row form-group" id="information">
							<div class="col-sm-4">
								<label for="namevisitor">Name</label>
								<input class="form-control" id="namevisitor" name="namevisitor" type="text" value="{{ old('namevisitor') }}">
								<div style="color:red;">@yield('namevisitor')</div>
							</div>
							<div class="col-sm-4">
								<label for="emailvisitor">Email</label>
								<input class="form-control" id="emailvisitor" name="emailvisitor" type="text" value="{{ old('emailvisitor') }}">
								<div style="color:red;">@yield('emailvisitor')</div>
							</div>
							<div class="col-sm-4">
								<label for="webvisitor">Web</label>
								<input class="form-control" id="webvisitor" name="webvisitor" type="text" value="{{ old('webvisitor') }}">
								<div style="color:red;">@yield('webvisitor')</div>
							</div>
						</div>

						<div class="row form-group" id="text">
							<div class="col-sm-12">
								<label for="messagevisitor">Message</label>
								<textarea class="form-control" rows="8" id="messagevisitor" name="messagevisitor">{{ old('messagevisitor') }}</textarea>
								<div style="color:red;">@yield('messagevisitor')</div>
							</div>
						</div>

						{!! csrf_field() !!}
						<div class="row form-group" id="buttons">
							<div class="col-sm-6 col-sm-offset-3">
								<button type="submit" class="btn btn-lg btn-success btn-block">Send Message</button>
							</div>
						</div>
					</form>
					@endif
					@if ($indexFields[0]['anewsletter'])
						<div class="col-sm-4 col-sm-offset-4">
							<button type="button" class="btn btn-lg btn-info btn-block" data-toggle="modal" data-target="#neswletter" data-whatever="Subscribe to Newsletter">Subscribe to Newsletter</button>
						</div>
					@endif	
				</div>
			</div>
		</article>
	</div>


@if ($indexFields[0]['anewsletter'])
	<div class="modal fade" id="neswletter" tabindex="-1" role="dialog" aria-labelledby="neswletterLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="neswletterLabel">Newsletter</h4>
				</div>
				<div class="modal-body">
					<form method="post" action="contact/email">
						<div class="form-group">
							<select class="form-control" id="typewilling" name="typewilling">
								<option>Subscribe</option>
								<option>Unsubscribe</option>
							</select>
							<div style="color:red;">@yield('typewilling')</div>
						</div>
						<div class="form-group">
							<label for="emailwilling">Email</label>
							<input class="form-control" id="emailwilling" name="emailwilling" type="text" value="{{ old('emailwilling') }}">
							<div style="color:red;">@yield('emailwilling')</div>
						</div>
						{!! csrf_field() !!}

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="Submit" class="btn btn-primary">Send</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endif
</body>
<script src="../js/main.js"></script>
</html>