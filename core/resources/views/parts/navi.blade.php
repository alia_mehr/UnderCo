<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="../admin">Admin Panel</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		@if (session('you.yourAdmin'))
			<ul class="nav navbar-nav">
				<li class="dropdown @yield('statusMnueSetting')">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Setting<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="@yield('statusSetting')"><a href="../admin/settings">Setting</a></li>
						<li class="@yield('statusUser')"><a href="../admin/user">User</a></li>
						<li class="@yield('statusAdduser')"><a href="../admin/adduser">Add User</a></li>
					</ul>
				</li>
				<li class="@yield('statusMessage')"><a href="../admin/messages">Message <span class="badge">{{ session('totalMsg') }}</span></a></li>
				<li class="@yield('statusEmail')"><a href="../admin/emails">Email <span class="badge">{{ session('totalMail') }}</span></a></li>
			</ul>
		@endif
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="resulttime badge">{{ session('timer') }}</span> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/"><span class="glyphicon glyphicon-eye-open"></span> Visit Site</a></li>
						@if (count(session('you')))
							<!-- <li class="@yield('statusHelp')"><a href="../admin/help"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li> -->
							<li><a href="../admin/logout"><span class="glyphicon glyphicon-log-out"></span> Exit</a></li>
						@endif
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>