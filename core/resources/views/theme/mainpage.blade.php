<!DOCTYPE html>
<html lang="en">
<head>
  <title>UnderCo | Admin Panel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/admin.css">
</head>
<body>
<section class="container-fluid">
	<header class="row" id="site-header">
		 @include('parts.navi')
	</header>

	<section class="row" id="site-body">
		@yield('body_page')
	</section>
  
  	<footer class="row" id="site-footer">
		<nav class="navbar-fixed-bottom">
			<div class="pull-right" id="ver">
				<a style="color:#000;text-decoration:none;" href="https://gitlab.com/alia_mehr/UnderCo">UnderCo one</a>
			</div>
		</nav>
	</footer>
	<input type="hidden" id="mainTimer" value="{{ session('maintimer') }}">
</section>
</body>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/admin.js"></script>
  <script src="../js/main.js"></script>
  <script type="text/javascript">@yield('js_script')</script>
</html>