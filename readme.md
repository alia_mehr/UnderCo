In the name of Allah
# UnderCo

UnderCo is a web application for when your site is not yet available and it is Under Construction. UnderCo is open source and free,This application is based on laravel framework.

## Some Features

UnderCo core functionality of a content management system to you offers. Features such as : 

1.Add/remove user

2.Receive email for send newsletter

3.Receive messages of user

... Plus many other features.

## Official Documentation

UnderCo documents can be found here [UnderCo homepage](https://gitlab.com/alia_mehr/UnderCo/wikis).

## Contributing

For contributing & develop UnderCo see here [UnderCo repository](https://gitlab.com/alia_mehr/UnderCo/tree/master).

## Issues

If the specific problem you encounter defined here [UnderCo issues](https://gitlab.com/alia_mehr/UnderCo/issues).

## License

The UnderCo is open-sourced web application licensed under the [MIT License](https://opensource.org/licenses/MIT).
